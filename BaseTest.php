<?php
class BaseTest
{
	protected $app;
	protected $practiceMode;
	protected $test;

	public function __construct(Application $app, array $test)
	{
		$this->app  = $app;
		$this->test = $test;
	}

	public function setPracticeMode()
	{
		$this->practiceMode = true;
	}

	public function getPracticeMode()
	{
		return $this->practiceMode === true;
	}
}



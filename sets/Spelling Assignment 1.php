<?php
return [
	'name' => 'Spelling Test #1',
	'description' => 'Words from a spelling test',
	'type' => 'spelling',
	'questions' => [
		'made',
		'make',
		'rain',
		'little',
		'tell',
		'after',
		'off',
		'old',
		'ride',
		'some',
		'them',
		'good'
	]
];
<?php
return [
	'name' => 'Spelling Test #2',
	'description' => 'Words from a spelling test',
	'type' => 'spelling',
	'questions' => [
		'favorite',
		'first',
		'second'
	]
];
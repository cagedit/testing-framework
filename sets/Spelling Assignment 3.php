<?php
return [
	'name' => 'Spelling Test #3',
	'description' => 'Words from a spelling test',
	'type' => 'spelling',
	'questions' => [
		'next',
		'because',
		'third'
	]
];
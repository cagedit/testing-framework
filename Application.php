<?php
class Application
{
	public function __construct()
	{
		// Register the autoloader
		spl_autoload_register(__NAMESPACE__ . '\Application::autoload');

		// Load all of the tests in the 'sets directory'
		$this->tests = $this->loadTests();
		$selection   = $this->getInput('Which test?');

		if (is_numeric($selection) && isset($this->tests[--$selection])) {
			$assignment = $this->tests[$selection];

			shuffle($assignment['questions']);

			$testName = ucfirst($assignment['type']) . 'Test';
			$test     = new $testName($this, $assignment);

			$this->println('Would you like practice mode?');
			$practice = $this->println('In practice mode you are given the answer. (Yes or No)');
			$practice = strtolower($this->getInput(''));

			system('clear');

			if ($practice === 'y' || $practice === 'yes') {
				$test->setPracticeMode();
			}

			$test->start($assignment['questions']);
		}

	}

	public function autoload($class)
	{
		$class = str_replace('\\', '/', $class);

		$file = __DIR__ . "/{$class}.php";
		if (file_exists($file)) {
			require_once($file);
		}

	}

	public function getInput($output)
	{
		echo $output . ' ';
		$handle = fopen ("php://stdin", "r");
		$input  = fgets($handle);
		$input  = trim($input);

		return $input;
	}

	public function println($message)
	{
		echo $message . "\n";
	}

	protected function loadTests()
	{
		$directory   = __DIR__ . '/sets/';
		$assignments = glob($directory . '*.php');

		foreach ($assignments as $i => $assignment) {
			$newAssignment = require_once($assignment);
			$this->println(++$i . ': ' . $newAssignment['name'] . ' - ' . $newAssignment['description']);
			$this->assignments[] = $newAssignment;
		}

		return $this->assignments;
	}

	public function say($message)
	{
		// `echo {$message}|espeak -v en+f4 -s130`;
		`echo {$message}|espeak -ven-us+f5 -s150`;

	}
}



new Application;
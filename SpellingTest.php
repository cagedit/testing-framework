<?php
class SpellingTest extends BaseTest
{
	public function start(array $questions)
	{
		foreach ($questions as $question) {
			$first    = true;
			$noAnswer = false;

			do {
				if (!$first && !$noAnswer) {
					$this->app->say('That is incorrect.');
				}


				if ($this->getPracticeMode()) {
					$this->app->say("The word is {$question} and it is spelled like this:");
					$this->hint($question);
				}

				$this->app->say('Spell ' . $question);

				$answer = $this->app->getInput('');


				if ($answer === '=') {
					foreach (range(1, 3) as $i) {
						$this->hint($question);
					}

					return $this->start($questions);
				}


				$noAnswer = empty($answer);


				$first   = false;
				$correct = $answer === $question;

				if ($correct) {
					$this->app->say('Correct!');
				}
			} while (!$correct);
		}

		$this->app->say('You have completed this test! Congratulations!');
	}

	protected function hint($question)
	{
		$question = str_split($question);

		foreach ($question as $letter) {
			$this->app->say($letter);
		}
	}
}